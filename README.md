## PostPulse

A mobile-first blogging application where users can register, login, and write posts.

#### What's Next

App dev has been stalling for a bit. Combing pieces together at the rate of drunk, high, and
sleepy, and horny. All to say, undisciplined.




**MVP**
- [wip] save content as html or markdown instead of raw text content
- [wip] Can click existing blogs to view them as rendered markdown from html
- [ ] Make x button into a Menu because it also needs to be editable for posts
- [ ] edit posts
- [ ] vercel path config - fixes refreshes on non-root prod URLs

- [ ] Supabase cloud function to purge isDeleted=true records after 15 days
- [ ] Autosave mechanism ?
- [ ] default view is personal feed if logged in, option for global feed (toggle in corner)
- [ ] Users can upload an image for a blog
  - [ ] Supabase Storage service
- [ ] Add `<ErrorBoundary />`, `<Suspense />`
- [ ] UI test for login page
- [ ] useIsMutation global indicator
- [ ] optimistic updates for trivial API mutation calls
- [x] fix cors again
- [x] delete posts (use isDeleted property)
  - [x] getAll filters isDeleted items on server
- [x] getAll posts (from all users, currently)
  - [x] fix getall list showing only the added entry (when DB shows reality)
- [x] ServerStateLoader for Pages
  - tried it, does not refresh after invalidated query
- [x] post title comes from first line of editor content
- [x] add posts
  - [x] hook needs auth info `authorId`
  - [x] API endpoint
- [x] fix supabase sasl error: ERROR (60057): SASL: SCRAM-SERVER-FINAL-MESSAGE: server signature is missing
- [x] fastify-cors
- [x] Fix vite config for shared TypeScript files
- [x] login buttons drawer UI is wrong af
- [x] rich-text editor
  - [x] react-draft-wsyiwyg breaks on mobile with some React.createElement issue
  - [x] tiptap docs/site is insufficient
  - [x] Editor.js does not have HTML ser/de yet
  - [x] Lexical.dev - ToolbarPlugin depends on outdated dep + not supported by devs
  - [x] Winner: Suneditor
    - [x] Get state from editor ~~(onSave?)~~ `useRef`
- [x] Put user auth source of truth in react-query store from server (duh)
- [x] Model for posts
- [x] Axios API client
- [x] register
  - [x] ~~Decide between fastify/passport and~~ Supabase Auth service
    - Using existing service to focus on app features
- [x] login
  - [x] Simple Login UI ~~page~~ drawer
- [x] HomePage is global feed of posts (opt-in to global feed post-by-post)
- [x] UI theme
  - https://coolors.co/palette/ccd5ae-e9edc9-fefae0-faedcd-d4a373
- [x] Configure TypeScript to recognize @shared directory

##### Questions / Thoughts

- Do most backend create tables in code or manually? Probably not manually
- Fastify-CLI does not refresh fastify app const as needed
- Mutations could scrap useMutation hook and just invalid in the .then
