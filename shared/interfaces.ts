export interface IPost {
  postId: number;
  /** Supabase AuthSession.data.user.id */
  authorId: string;
  content: string;
  title: string;
  slug: string;
  createdAt: number;
  updatedAt: number;
  isDeleted: boolean;
}

export type IPostCreate = Pick<IPost, "authorId" | "content" | "title">
export type IPostUpdate = Partial<IPost> & Pick<IPost, "postId">

export enum ApiPaths {
  Posts = '/posts',
}
