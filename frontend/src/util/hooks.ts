import { useSnackbar, OptionsObject } from "notistack";
import { useCallback, useState } from "react";

/**
 * @usage
 * ```TypeScript
 * const toast = useToast();
 * toast.error('My message here!')
 * ```
 */
export const useToast = () => {
  const { enqueueSnackbar, closeSnackbar } = useSnackbar();

  const info = useCallback(
    (msg: string, opts: OptionsObject = { variant: "info" }) => {
      return enqueueSnackbar(msg, opts);
    },
    [enqueueSnackbar],
  );

  const error = useCallback(
    (msg: string, opts: OptionsObject = { variant: "error" }) => {
      if (process.env.NODE_ENV === "development") {
        console.error(msg);
      }
      return enqueueSnackbar(msg, opts);
    },
    [enqueueSnackbar],
  );

  return { info, error, closeSnackbar };
};

/**
 * For use with MUI drawers and menu items
 */
export const useDrawer = <T>() => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [data, setData] = useState<T | null>(null);

  const openMenu = useCallback(
    (event: React.MouseEvent<HTMLElement>) => setAnchorEl(event.currentTarget),
    [],
  );

  const closeMenu = useCallback(() => {
    setAnchorEl(null);
    setData(null);
  }, []);

  const getMenu = useCallback(
    () => ({
      open: !!anchorEl,
      onOpen: openMenu,
      onClose: closeMenu,
    }),
    [anchorEl, closeMenu, openMenu],
  );

  return {
    ...getMenu(),
    anchorEl,
    props: getMenu,
    onOpen(event: React.MouseEvent<HTMLElement>, data: T) {
      setAnchorEl(event.currentTarget);
      setData(data);
    },
    getData: () => data,
    setData,
  };
};
