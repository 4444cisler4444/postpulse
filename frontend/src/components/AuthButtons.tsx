import { PersonAddOutlined } from "@mui/icons-material";
import { Box, Button, Stack, SwipeableDrawer, Typography } from "@mui/material";
import { ThemeSupa } from "@supabase/auth-ui-shared";
import { Auth } from "@supabase/auth-ui-react";

import { St8, useDrawer } from "../util";
import { supabase, useUser } from "../api";

export const AuthButtons = () => {
  const authDrawer = useDrawer();
  const user = useUser();

  // useEffect(() => {
  //   const {
  //     data: { subscription },
  //   } = auth.onAuthStateChange((_event, session) => {
  //     queryClient.setQueryData(queries.auth.user.queryKey, session);
  //   });

  //   return () => subscription.unsubscribe();
  // }, []);

  return (
    <>
      {St8.isReady(user) ? (
        <Typography>{user.id.slice(0, 8)}</Typography>
      ) : (
        <Stack direction="row" spacing={1}>
          <Button
            onClick={(event) => {
              authDrawer.onOpen(event, null);
            }}
          >
            Login
          </Button>
          <Button
            variant="contained"
            endIcon={<PersonAddOutlined />}
            onClick={(event) => {
              authDrawer.onOpen(event, null);
            }}
          >
            Signup
          </Button>
        </Stack>
      )}

      <SwipeableDrawer {...authDrawer.props()} anchor="right">
        <Box
          sx={
            {
              // width: "100%",
              // maxWidth: (theme) => theme.breakpoints.values.sm,
              // position: "absolute",
              // padding: "0 1rem",
              // top: "50%",
              // left: "50%",
              // transform: "translate(-50%, -50%)",
            }
          }
        >
          <Auth
            supabaseClient={supabase}
            appearance={{ theme: ThemeSupa }}
            providers={["google"]}
          />
        </Box>
      </SwipeableDrawer>
    </>
  );
};
