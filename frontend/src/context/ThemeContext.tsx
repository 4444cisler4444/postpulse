import { CssBaseline, useMediaQuery } from "@mui/material";
import {
  createTheme,
  responsiveFontSizes,
  ThemeProvider,
} from "@mui/material/styles";
import { FC, ReactNode } from "react";

// https://mui.com/material-ui/customization/theming/#typescript
declare module "@mui/material/styles" {
  interface Theme {}
  interface ThemeOptions {}
}

// https://coolors.co/palette/ccd5ae-e9edc9-fefae0-faedcd-d4a373
// CCD5AE -> E9EDC9 -> FEFAE0 -> FAEDCD -> D4A373
export const ThemeContext: FC<{ children: ReactNode }> = ({ children }) => {
  const prefersDark = useMediaQuery("@media (prefers-color-scheme: dark)");

  const theme = createTheme({
    palette: {
      mode: prefersDark ? "dark" : "light",
      primary: {
        main: "#D4A373",
      },
      secondary: {
        main: "#E9EDC9",
      },
    },
    typography: {
      fontFamily: "Inter, sans-serif",
    },
    components: {
      MuiButtonBase: {
        defaultProps: {
          disableRipple: true,
          disableTouchRipple: true,
          sx: {
            borderRadius: "0 !important",
            textTransform: "none",
          },
        },
      },
      MuiSwipeableDrawer: {
        defaultProps: {
          PaperProps: {
            sx: {
              padding: (theme) => theme.spacing(3, 2, 6, 2),
              maxWidth: (theme) => theme.breakpoints.values.sm,
              margin: "0 auto",
            },
          },
        },
      },
    },
  });

  return (
    <ThemeProvider theme={responsiveFontSizes(theme)}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
};
