import { Box, Button, IconButton, Stack, Typography } from "@mui/material";
import { Close, EditNote } from "@mui/icons-material";
import { useCallback, useRef } from "react";
import SunEditor from "suneditor-react";
import SunEditorCore from "suneditor/src/lib/core";
import "suneditor/dist/css/suneditor.min.css";

import { usePostActions, usePosts, useUser } from "../api";
import { AuthButtons } from "../components";
import { St8, St8View, useToast } from "../util";

export const HomePage = () => {
  // Only way to extract value when creating a post
  const editorRef = useRef<SunEditorCore>();
  const toast = useToast();
  // Get cached auth state
  const user = useUser();

  // Fetch list of posts
  const posts = usePosts();
  const postActions = usePostActions();

  const handleCreatePost = useCallback(async () => {
    if (!St8.isReady(user)) {
      toast.info("Login to write posts!");
      return;
    }
    // grab content and verify
    const content = editorRef.current?.getContents(false) ?? "";
    console.log({ content });
    const title = content.split(/\r/g)[0] || "Untitled";
    if (!content) {
      toast.info("Post cannot be empty.");
      return;
    }
    try {
      // create entry, loading indicators auto-happen
      const post = await postActions.create({
        authorId: user.id,
        content,
        title,
      });
      document
        .getElementById(post.postId + "")
        ?.appendChild(
          React.createElement("div", {
            dangerouslySetInnerHTML: { __html: content },
          }),
        );

      toast.info("Post posted to PostPulse.");
    } catch (err) {
      toast.error(err instanceof Error ? err.message : String(err));
    }
  }, [postActions, toast, user]);

  return (
    <Stack
      spacing={2}
      sx={{
        height: "100vh",
        width: "100vw",
        maxWidth: (theme) => theme.breakpoints.values.md,
        margin: "0 auto",
      }}
    >
      <Stack direction="row" justifyContent="space-between" padding={1}>
        <Typography
          variant="overline"
          sx={{ textShadow: "1px 1px 2px grey" }}
          color="text.secondary"
        >
          <em>PostPulse</em>
        </Typography>
        <Box />
        <AuthButtons />
      </Stack>
      {/** Header button to go back to list of things */}
      {/** One button to open writing setting menu */}
      {/** h1, h3, h5, body, monospace/code */}
      {/** bold, italic, underline, strikethrough */}
      <Stack spacing={3} sx={{ padding: (theme) => theme.spacing(0, 2) }}>
        <SunEditor
          placeholder="Write a post..."
          getSunEditorInstance={(instance) => {
            editorRef.current = instance;
          }}
          setOptions={{
            // TODO balloon
            display: "balloon",
            showPathLabel: false,
            height: "100%",
            defaultStyle: "font-size: 1rem",
            popupDisplay: "local",
            buttonList: [
              [
                "undo",
                "redo",
                // "font",
                "bold",
                "underline",
                "italic",
                "strike",
                // "fontSize",
                "align",
                "list",
                // "indent",
                // "outdent",
              ],
            ],
          }}
        />
        <Box>
          <Button
            variant="contained"
            startIcon={<EditNote />}
            onClick={handleCreatePost}
          >
            Add Post
          </Button>
        </Box>
      </Stack>
      <St8View
        data={posts}
        loading={() => <Typography>Loading...</Typography>}
        error={() => <Typography>Error</Typography>}
        empty={() => <Typography>Empty</Typography>}
      >
        {(posts) => (
          <Stack spacing={1}>
            {posts.map((post) => (
              <Box
                key={post.postId}
                display="flex"
                justifyContent="space-between"
                sx={(theme) => ({
                  backgroundColor: theme.palette.background.paper,
                  borderRadius: theme.shape.borderRadius,
                  padding: theme.spacing(2),
                  marginBottom: theme.spacing(1),
                  boxShadow: theme.shadows[5],
                })}
              >
                <Stack>
                  <Typography>{post.title}</Typography>
                  <Typography color="text.secondary" variant="body2">
                    {post.content.split("\n").slice(1).join("\n")}
                  </Typography>
                </Stack>
                <IconButton
                  onClick={async () => {
                    try {
                      await postActions.delete(post);
                      toast.info(`Post deleted ID: ${post.postId}`);
                    } catch (err) {
                      toast.error(
                        err instanceof Error ? err.message : String(err),
                      );
                    }
                  }}
                >
                  <Close fontSize="small" />
                </IconButton>
              </Box>
            ))}
          </Stack>
        )}
      </St8View>
    </Stack>
  );
};
