import { Navigate, Route, Routes } from "react-router-dom";

import { RoutePaths } from "./util";
import { HomePage } from "./pages";

export const App = () => {
  return (
    <Routes>
      <Route path={RoutePaths.posts} element={<HomePage />} />
      <Route path="*" element={<Navigate to={RoutePaths.posts} />} />
    </Routes>
  );
};
