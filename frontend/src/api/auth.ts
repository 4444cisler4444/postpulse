import { createClient } from "@supabase/supabase-js";

const supabaseKey =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImZ6ZmNxZHhtcmxsdGxudHJ3anVzIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDg4MDMyOTgsImV4cCI6MjAyNDM3OTI5OH0.N5qTFFqEqLpwLlAs8ZsXpUlTUFXggjpu6F96_1atv1s";
const supabaseUrl = "https://fzfcqdxmrlltlntrwjus.supabase.co";

export const supabase = createClient(supabaseUrl, supabaseKey);

export const auth = supabase.auth;
