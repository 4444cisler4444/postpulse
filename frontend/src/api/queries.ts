import { createQueryKeyStore } from "@lukemorales/query-key-factory";
import { QueryClient, useQuery } from "@tanstack/react-query";
import { AuthError, Session } from "@supabase/supabase-js";

import { auth } from "./auth";
import { St8 } from "../util";
import { api } from "./api";

export const queries = createQueryKeyStore({
  auth: {
    session: {
      queryKey: null,
      queryFn: () => auth.getSession(),
    },
  },
  posts: {
    // Currently not dependent on user ID
    all: {
      queryKey: null,
      queryFn: () => api.posts.get("/").then((_) => _.data),
    },
  },
});

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: 1,
    },
  },
});

// Every page depends on auth info
queryClient.prefetchQuery(queries.auth.session).catch((err) => {
  console.error(err);
});

export const useUser = () => {
  // Get auth session (from network or cache if prefetch returned)
  // Wish Supabase had types for this response
  const authReplyState = St8.from<{
    error: null | AuthError;
    data: { session: null | Session };
  }>(useQuery(queries.auth.session));

  // Flatten response object into isLoading/error/sessionData
  return St8.map(authReplyState, ({ data, error }) => {
    // Failed to get login state
    if (error) return St8.error(error);
    // Not logged in
    if (!data.session) return St8.Empty;
    // Is logged in, give 'em teh datas
    return data.session.user;
  });
};
