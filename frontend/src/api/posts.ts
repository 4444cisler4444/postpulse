import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { useMemo } from "react";

import { IPostCreate, IPost, IPostUpdate } from "@shared-ts";
import { queries } from "./queries";
import { api } from "./api";
import { St8 } from "../util";

/**
 * Gets from cache or network a list of posts.
 */
export const usePosts = (): St8<IPost[]> => {
  return St8.from(useQuery(queries.posts.all));
};

/**
 * Actions on the list which modify inner elements.
 */
export const usePostActions = () => {
  const client = useQueryClient();

  const onSuccess = () => {
    client.invalidateQueries({ queryKey: queries.posts.all.queryKey });
  };

  const { mutateAsync: createPost } = useMutation({
    async mutationFn(fields: IPostCreate): Promise<IPost> {
      const reply = await api.posts.post("/", fields);
      return reply.data;
    },
    onSuccess,
  });

  const { mutateAsync: updatePost } = useMutation({
    async mutationFn(fields: IPostUpdate): Promise<IPost> {
      const reply = await api.posts.put(`/${fields.postId}`, fields);
      return reply.data;
    },
    onSuccess,
  });

  const { mutateAsync: deletePost } = useMutation({
    async mutationFn({ postId }: Pick<IPost, "postId">): Promise<number> {
      const reply = await api.posts.delete(`/${postId}`);
      // Remove deleted item from cache
      client.setQueryData(queries.posts.all.queryKey, (posts: IPost[]) => {
        return posts.filter((_) => _.postId !== postId);
      });
      // Returns a count of the number of deleted items, which is one
      return reply.data;
    },
    // onSuccess,
  });

  return useMemo(
    () => ({
      create: createPost,
      update: updatePost,
      delete: deletePost,
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [],
  );
};
