import axios from "axios";

import { ApiPaths } from "@shared-ts";

const BASE_URL =
  process.env.NODE_ENV === "development"
    ? "http://localhost:3000/api/v1"
    : "No Production URL";

export const api = {
  posts: axios.create({
    baseURL: BASE_URL,
    url: ApiPaths.Posts,
    withCredentials: true,
  }),
};
