import { FastifyPluginAsync, FastifyRequest } from "fastify";
import slugify from "slugify";

import { IPostCreate, IPost } from "@shared-ts/interfaces";
import { db } from "../../../db.js";

const posts: FastifyPluginAsync = async (fastify, _opts) => {
  fastify.post<{ Body: IPostCreate; Reply: IPost }>(
    "/",
    {
      schema: {
        body: {} as IPostCreate,
        response: {
          201: {} as IPost,
        },
      },
    },
    async function CreatePost(request, _reply) {
      const { authorId, content, title } = request.body;
      const slug = slugify.default(title, {
        lower: true,
        remove: /[*+~.()'"!:@]/g,
      });
      const now = Date.now();
      const rows = await db("posts")
        .insert({
          authorId,
          content,
          title,
          slug,
          createdAt: now,
          updatedAt: now,
        })
        .returning("*");
      if (rows.length === 0) throw Error("Could not create post.");
      return rows[0];
    },
  );

  fastify.get(
    "/",
    // How to??
    // { schema: { response: { 200: {} as IPost[] } } },
    async function GetAllPosts(request, reply) {
      const rows = await db("posts")
        .select("*")
        .where({ isDeleted: false })
        .orderBy("createdAt", "desc")
        .limit(13);
      return rows;
    },
  );

  fastify.put("/:postId", async function UpdatePostById(request, reply) {
    // TODO
  });

  fastify.delete(
    "/:postId",
    {
      schema: {
        params: { type: "object", properties: { postId: { type: "number" } } },
      },
    },
    async function DeletePostById(
      request: FastifyRequest<{ Params: { postId: number } }>,
      _reply,
    ) {
      const postId = request.params.postId;
      const { length: count } = await db("posts")
        .update({ isDeleted: true })
        .where({ postId })
        .returning("postId");
      return { count };
    },
  );
};

export default posts;
