import createKnex from "knex";

import { ApiPaths, IPost } from "@shared-ts/interfaces";

console.log("DB Connection string ----->>", process.env.PG_CONNECTION_STRING);

const snakecase = (str: string): string =>
  str.replace(/([A-Z])/g, "_$1").toLowerCase();

const camelcase = (str: string): string =>
  str.replace(/_([a-z])/g, (g) => g[1].toUpperCase());

const camelcaseObj = (row: object): object =>
  Object.fromEntries(
    Object.entries(row).map(([key, value]) => [camelcase(key), value]),
  );

/**
 * Our database query builder.
 */
const knex = createKnex({
  client: "pg",
  connection: process.env.PG_CONNECTION_STRING,
  searchPath: ["public"],
  postProcessResponse(snapshot, _queryContext) {
    if (Array.isArray(snapshot)) {
      return snapshot.map(camelcaseObj);
    }
    if (!!snapshot && typeof snapshot === "object") {
      return camelcaseObj(snapshot);
    }
    return snapshot;
  },
  wrapIdentifier: (value, origImpl, _queryContext) => {
    const converted = snakecase(value);
    return origImpl(converted);
  },
});

type Slashless<T extends string> = T extends `/${infer Path}` ? Path : never;

export function db(path: Slashless<ApiPaths>) {
  if (path === "posts") return knex<IPost>(path);
  throw TypeError(`Unknown path: ${path}`);
}
